
const Post = require('./../Models/postModel')
const { SUCCESS, NOT_FOUND, BAD_REQUEST } = require('../Messages/httpCode')

module.exports.Index = async (req, res) => {
  try {
    const post = await Post.find({});
    res.status(SUCCESS.httpCode)
      .json(
        {
          messageCode: SUCCESS.messageCode,
          message: SUCCESS.message,
          data: post
        }
      )
  } catch (err) {
    res.status(NOT_FOUND.httpCode).json({
      messageCode: NOT_FOUND.messageCode,
      message: NOT_FOUND.message
    })
  }
}
module.exports.Store = (req, res) => {
  Post.create(req.body, err => {
    if (err) {
      res.status(BAD_REQUEST.httpCode)
        .json(
          {
            messageCode: BAD_REQUEST.messageCode,
            message: err
          }
        );
    } else {
      res.status(SUCCESS.httpCode)
        .json(
          {

            messageCode: SUCCESS.messageCode,
            message: SUCCESS.message
          }
        )
    }

  })
}
module.exports.Update = async (req, res) => {
  const postId = req.params.postId;
  await Post.findOneAndReplace({ _id: postId }, req.body, { new: true })
    .then(res => {
      res.status(SUCCESS.httpCode).json({
        messageCode: SUCCESS.messageCode,
        message: SUCCESS.message,
      });
    })
    .catch(err => {
      res.status(NOT_FOUND.httpCode).json({
        messageCode: NOT_FOUND.messageCode,
        message: err
      });
    });
}

module.exports.Edit = async (req, res) => {
  const postId = req.params.postId;
  await Post.findOneAndUpdate({ _id: postId }, { $set: req.body }, { new: true }, err => {
    if (err) {
      res.status(NOT_FOUND.httpCode).json({
        messageCode: NOT_FOUND.messageCode,
        message: err
      })
    } else {
      res.status(SUCCESS.httpCode).json({
        messageCode: SUCCESS.messageCode,
        message: message +  postId

      })
    }
  })
}

module.exports.Delete = async (req, res) => {
  const postId = req.params.postId;
  await Post.countDocuments({ _id: postId }, function (err, count) {
    if (count > 0) {
      Post.findOneAndDelete({ _id: postId }, err => {
        if (err) {
          res.status(NOT_FOUND.httpCode).json({
            messageCode: NOT_FOUND.messageCode,
            message: err
          })
        } else {
          res.status(SUCCESS.httpCode).json({
            messageCode: SUCCESS.messageCode,
            message: SUCCESS.message +  postId,

          })
        }
      })
    } else {
      res.status(NOT_FOUND.httpCode)
        .json({
          messageCode: NOT_FOUND.messageCode,
          message: NOT_FOUND.message +  postId
        })
    }
  });


}