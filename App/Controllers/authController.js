const jwt = require('jsonwebtoken')
const Auth = require('./../Models/authModel')
const { SUCCESS, NOT_FOUND, USER_NOT_FOUND, USER_EMAIL_INVALID, LOGIN_FAIL } = require('../Messages/httpCode')

// Controllers 
function signJWT(options, keywords) {
    return jwt.sign(options, keywords)
}
module.exports.getDataAuth = async (req, res) => {
    const email = req.body.email;
    const password = req.body.password;
    if (!email) {
       return res.status(USER_EMAIL_INVALID.httpCode).json({
            messageCode: USER_EMAIL_INVALID.messageCode,
            message: USER_EMAIL_INVALID.message
        })

    }
    await Auth.findOne({ email }, (err, user) => {
        if (err) {
          return res.status(NOT_FOUND.httpCode).json({
                messageCode: NOT_FOUND.messageCode,
                message: err
            })
        }
        if (!user) {
           return res.status(USER_NOT_FOUND.httpCode).json(
                {
                    messageCode: USER_NOT_FOUND.messageCode,
                    message: USER_NOT_FOUND.message
                }
            )
        } else if (password !== user.password) {
           return res.status(LOGIN_FAIL.httpCode).json(
                {
                    messageCode: LOGIN_FAIL.messageCode,
                    message: LOGIN_FAIL.message
                }
            )
        } else {
            return res.status(SUCCESS.httpCode)
                .json({
                    messageCode: SUCCESS.messageCode,
                    message: SUCCESS.message,
                    token: signJWT({ email: user.email, _id: user._id }, process.env.SIGNATURE_API)
                }
                )
        }
    })

}
