const SUCCESS = {
    httpCode: 200,
    messageCode: 'SUCCESS',
    message: 'Success',
  };
  
  const BAD_REQUEST = {
    httpCode: 400,
    messageCode: 'BAD_REQUEST',
    message: 'Bad request',
  };
  
  const NOT_FOUND = {
    httpCode: 404,
    messageCode: 'NOT_FOUND',
    message: 'The record does not existed or removed',
  }
  
  const USER_NOT_FOUND = {
    httpCode: NOT_FOUND.httpCode,
    messageCode: NOT_FOUND.messageCode,
    message: 'User does not existed. Please contact to support',
  }
  
  const USER_REQUIRED_ERROR = {
    httpCode: BAD_REQUEST.httpCode,
    messageCode: BAD_REQUEST.messageCode,
    message: (fields = []) => `${fields.join(',')} is required`,
  }
  
  const USER_EMAIL_INVALID = {
    httpCode: BAD_REQUEST.httpCode,
    messageCode: BAD_REQUEST.messageCode,
    message: 'Email is invalid format',
  }
  
  const LOGIN_REQUIRED = {
    httpCode: BAD_REQUEST.httpCode,
    messageCode: BAD_REQUEST.messageCode,
    message: 'Email & password is required',
  }
  
  const LOGIN_FAIL = {
    httpCode: 401,
    messageCode: 'UNAUTHENTICATION',
    message: 'Email or password is wrong',
  }
  
  const UNAUTHENTICATION = {
    httpCode: 401,
    messageCode: 'UNAUTHENTICATION',
    message: 'Unauthentication',
  }
  
  module.exports = {
    SUCCESS,
    BAD_REQUEST,
    NOT_FOUND,
    USER_NOT_FOUND,
    USER_REQUIRED_ERROR,
    USER_EMAIL_INVALID,
    LOGIN_FAIL,
    UNAUTHENTICATION,
    LOGIN_REQUIRED,
  }