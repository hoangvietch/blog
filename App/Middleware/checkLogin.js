const jwt = require('jsonwebtoken')
const { UNAUTHENTICATION } = require('../Messages/httpCode')

module.exports.checkAuthLogin = (req, res, next) => {
  const token = req.headers.token;
  try {
    jwt.verify(token, process.env.SIGNATURE_API);
    return next();
  } catch (err) {
    res.status(UNAUTHENTICATION.httpCode)
      .json({
        messageCode: UNAUTHENTICATION.messageCode,
        message: err
      })
  }

}
