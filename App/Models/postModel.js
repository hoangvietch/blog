var mongoose = require('mongoose');
var uniqueValidator = require('mongoose-unique-validator');

var Schema = mongoose.Schema;

var postSchema = new Schema({
  name: {type: String, required: true},
  slug: {type: String, index: true, required: true, unique: true},
  description: {type: String},
  image: {type: String},
  author: String,
  timestamp: {
    created_at: { type: Date, default: Date.now },
    updated_at: {type: Date, default: Date.now }

  }
 
});
postSchema.plugin(uniqueValidator);
var Post = mongoose.model('Post', postSchema);
module.exports = Post;
