var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var authSchema = new Schema({
  name: {type: String, required: true},
  username: {type: String, required: true, unique: true},
  email: {type: String, required: true, unique: true},
  password: {type: String, required: true},
  avatar: String,
  created_at: { type: Date, default: Date.now },
 
});
var Auth = mongoose.model('Auth', authSchema);
module.exports = Auth;