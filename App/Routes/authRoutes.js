const express = require('express')
const router = express.Router()

// Middlewares
const middlewares = require('./../Middleware/checkLogin')
// Controllers
const controllers = require('./../Controllers/authController')

router.post('/auth/login', controllers.getDataAuth);
module.exports = router;