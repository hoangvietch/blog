const express = require('express')
const router = express.Router()

// Controllers
const controllers = require('../Controllers/postController')


router.get('/post', controllers.Index) // Get all list post
router.post('/post', controllers.Store) // Create a new post
router.put('/post/:postId', controllers.Update) // Replace post 
router.patch('/post/:postId', controllers.Edit) // Edit filed post
router.delete('/post/:postId', controllers.Delete) // Delete post

module.exports = router;

