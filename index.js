require('dotenv').config()
require('./App/Config/db')
const express = require('express')
const app = express()
var cors = require('cors')
app.use(cors())
const bodyParser = require('body-parser')
const { APP_PORT, API_URL } = process.env;

const middlewares = require('./App/Middleware/checkLogin');
// Router import here * //

const authRouter = require(`./App/Routes/authRoutes`)
const postRouter = require('./App/Routes/postRoutes')

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: true,
}));

app.use(API_URL, authRouter, middlewares.checkAuthLogin, postRouter)

app.listen(APP_PORT, () => {
    console.log(`App listening on port ${APP_PORT}`)
})
